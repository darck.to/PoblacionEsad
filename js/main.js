(function() {
    // Menu
    $('nav').load('./templates/menu/menu.html', function(e) {
      // Maindoby empty
      $('#mainBody').html('');
    })


  //CREA UN TOAST MODAL
  window.toast = function(txt,type) {
    if (!$('#toast').length) {
      $('body').append('<div id="toast"></div>');
    }
    if (!type)
        type = 'info';
    // Numero para el ID
    // Numero para el ID
    let rndNmr = (Math.random() + 1).toString(36).substring(7);
    var toast = '<span id="tag-' + rndNmr + '" class="has-background-info has-text-white is-size-7 m-2 p-2 has-background-' + type + ' toast-tag">' + txt + '</span>';
    $('#toast').append(toast);
    setTimeout(function(){
      setTimeout(function(){
        $('body').find('#tag-' + rndNmr).fadeOut(600,function(){   // Slideup callback executes AFTER the slideup effect.
          $(this).remove();
        });
      },4000)
    })
  }

  //CREA UN MODAL PARA TODOS Y TODO
  window.modal = function(incoming) {
    //DESTRUYE VIEJOS MODALES
    if ($('.modal').length) {
      $('.modal').fadeOut('fast', function() {
        $(this).remove()
      })
    }
    //CREA UN MODAL NUEVO
    var content = incoming;
    var modal = '<div class="modal">';
    modal += '<div class="modal-background"></div>';
    modal += '<div class="modal-content">';
      modal += '<div class="card">';
        modal += content;
      modal += '</div>';
    modal += '</div>';
    modal += '<div class="modal-absolute"></div>';
    modal += '<i class="fa-solid fa-circle-xmark modal-exit fa-2x has-text-white-ter is-clickable"></i>';
    modal += '</div>';
    $('body').prepend(modal);
    $('.modal').css('z-index','9000');
    $('.modal').toggleClass("is-active");
    $('.modal-exit').on('click', function(){
      $('.modal').fadeOut('fast', function() {
        $(this).remove()
      })
    });
    $('.modal-background').on('click', function(){
      $('.modal').fadeOut('fast', function() {
        $(this).remove()
      })
    })
  }
  //MODAL SOLO CLAUSURABLE CON BOTON
  window.modap = function(incoming) {
    //DESTRUYE VIEJOS MODALES
    if ($('.modal').length) {
      $('.modal').fadeOut('fast', function() {
        $(this).remove()
      })
    }
    //CREA UN MODAL NUEVO
    var content = incoming;
    var modal = '<div class="modal">';
    modal += '<div class="modal-background"></div>';
      modal += '<div class="modal-content">';
        modal += '<div class="card">';
          modal += content;
        modal += '</div>';
      modal += '</div>';
      modal += '<div class="modal-absolute"></div>';
      modal += '<i class="fa-solid fa-circle-xmark modal-exit fa-2x has-text-white-ter is-clickable"></i>';
    modal += '</div>';
    $('body').prepend(modal);
    $('.modal').css('z-index','9000');
    $('.modal').toggleClass("is-active");
    $('.modal-exit').on('click', function() {
      $('.modal').fadeOut('fast', function() {
        $(this).remove()
      })
    })
  }
  //CIERRAMODAL
  window.modax = function(e) {
    //DESTRUYE VIEJOS MODALES
    if ($('.modal').length) {
      $('.modal').fadeOut('fast', function() {
        $(this).remove()
      })
    }
  }

  //Breadcrumb
  window.bread = function(first) {
    var template = '<li class="is-active"><a aria-current="page">' + first.charAt(0).toUpperCase() + first.slice(1) + '</a></li>';
    var content = template;
    var bread = '<nav class="breadcrumb" aria-label="breadcrumbs">';
      bread += '<ul>';
        bread += content;
      bread += '</ul>';
    bread += '</nav>';
    $('#breadcrumbs').html(bread);
  }

  // Is loading spinner
  window.isloading = function(type) {
    if (type == 1) {
      // Add is-loading class to stop user from clicking hyperlinks, inputs and buttons 
      $('button').addClass('is-loading');
      $('textarea').addClass('is-loading');
      $('input').addClass('is-loading');
      $('a').addClass('is-disabled');
    } else if (type == 0) {
      // remove is-loading class to stop user from clicking hyperlinks, inputs and buttons 
      $('button').removeClass('is-loading');
      $('textarea').removeClass('is-loading');
      $('input').removeClass('is-loading');
      $('a').removeClass('is-disabled');
    }
  };

}());