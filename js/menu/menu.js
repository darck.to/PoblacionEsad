(function() {
    // nav-elements
    $('.nav-element').on('click', function(e) {
        // avoid default
        e.preventDefault();
        // Remove active class
        $('.nav-element').removeClass('is-active');
        // Add active class
        $(this).addClass('is-active');
        // If resoultion is mobile
        if ($(window).width() < 824) {
            $('.sidebar-v1').toggleClass('is-fold')
        }
        // Clean mainBody DOM
        $('#mainBody').html('');
        $('#mainBody').empty();
        // This href value
        let ruta = $(this).data('ruta');
        let href = $(this).data('url');
        // Load content
        $('#mainBody').load('./templates/' + ruta + '/' + href);
        // Breadcrumb
        bread(ruta);
        
    });
    // Cuando carga menu continuamos con el resto de secciones de principal
    $('#mainBody').load("./templates/portada/portada.html", function (e) {
        bread('Portada')
    });
    // Oculta el menu cuando se hace click un un boton
    $('.nav-toggler').on('click', function(e) {
        $('.sidebar-v1').toggleClass('is-fold');
        $('.view-wrapper').toggleClass('has-width-100 ml-0');
    })
  
}());