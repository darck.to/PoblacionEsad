// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.23.0/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyC-bZu5_F00-Fmf4L6JgPF0wk4v8VjkoN4",
    authDomain: "testrealdb-17286.firebaseapp.com",
    projectId: "testrealdb-17286",
    storageBucket: "testrealdb-17286.appspot.com",
    messagingSenderId: "974668284905",
    appId: "1:974668284905:web:4b352bdf6f22d3b01c0178"
};
// Initialize Firebase
const app = initializeApp(firebaseConfig);

import { getDatabase, ref, child, onValue, get, query, limitToFirst, limitToLast, orderByChild, orderByKey, orderByValue, startAt, startAfter, endAt, endBefore, equalTo } from "https://www.gstatic.com/firebasejs/9.23.0/firebase-database.js";

// Get a reference to the database service
const db = getDatabase();

// documento on ready
$(function() {
    
    // cargar datos
    cargarDatos();

    // Return Button regresa a cargarDatos()
    $('#returnButton').on('click', function(e) {
        cargarDatos();
        $(this).addClass('is-hidden');
    });

    // Show modal when a row is clicked
    $('#resultadosTabla tbody').on('click', 'td.is-clickable', function() {
        const field = $(this).attr('data-field');
        const id = $(this).attr('data-id');
        cargaTableDatos(field,id);
    });
    
    // evento Datos para resultadosTabla thead y tbody
    function cargarDatos() {
        isloading(1);
        // Ocualtar card-info
        $('.card-info').addClass('is-hidden');
        const que = query(ref(db, 'Paises'));

        get(que).then((snapshot) => {
            if (snapshot.exists()) {
                console.log(snapshot.val());
                const data = snapshot.val();
                // do a vertical table
                let head = '<tr>';
                let rows = '';
                // Add a header row
                    head += '<th>Country Code</th>';
                $.each(data, function(key, value) {
                    head += `<th class="is-uppercase">${key}</th>`; 
                });
                head += '</th>';
                // get all index from data.paises
                const claves = Object.keys(data.country);
                // get all value from data.paises
                const nombres = Object.values(data.country);
                // get all value from data.income
                const incomes = Object.values(data.income);
                // get all value from data.region
                const regions = Object.values(data.region);
                // Add a row for each index from data.paises
                $.each(claves, function(index, content) {
                    let clave, nombre = '<td></td>', income = '<td></td>', region = '<td></td>';
                    rows += '<tr>';
                        clave = `<td class="has-text-weight-bold">${content}</td>`;
                    $.each(data.country, function(key, value) {
                        if (content == key) {
                            nombre = `<td class="is-clickable" data-id="${key}">${value}</td>`;
                            return;
                         }
                    });
                    $.each(data.income, function(key, value) {
                        if (content == key) {
                            income = `<td class="is-clickable" data-id="${key}">${value}</td>`;
                            return;
                         }
                    });
                    $.each(data.region, function(key, value) {
                        if (content == key) {
                            region = `<td class="is-clickable" data-id="${key}">${value}</td>`;
                            return;
                         }
                    });
                        rows += clave + nombre + income + region;
                    rows += '</tr>';
                });

                // Add rows to table
                $('#resultadosTabla thead').html(head);
                $('#resultadosTabla tbody').html(rows);
                // Datatables 
                const tablePaises = $('#resultadosTabla').DataTable({
                    "lengthMenu": [ 100, 200 ],
                    buttons: [
                        'copy', 'excel'
                    ]
                });
                // Add buttons to card-header icons and add is-small class to each button in it
                //tablePaises.buttons().container().appendTo('.header-miembros-buttons');
                $('#resultadosTabla button').addClass('is-small');
                isloading(0);
            } else {
                isloading(0);
                toast("No data available");
            }
        }, {
            onlyOnce: true
        }).catch((error) => {
            console.error(error);
        });
    }

    function cargaTableDatos(field,id) {
        let querySrc = '_TOTL';
        if (field == 'Income_Group' || field == 'Region')
            querySrc = '_Metadata';
        const que = query(ref(db, querySrc), orderByChild(field), equalTo(id));

        get(que).then((snapshot) => {
            if (snapshot.exists()) {
                // Activate return button
                $('#returnButton').removeClass('is-hidden');
                const data = snapshot.val();
                let head = '';
                let rows = '';
                // Si es TOTL o META
                switch (querySrc) {
                    case '_TOTL':
                        head += '<tr>';
                        let headToSort = [];
                        $.each(data, function(key, value) {
                            for (const key in value) {
                                headToSort.push(key);
                            }
                        });
                        // Ordenamos en orden alfabetico inverso
                        headToSort.sort().reverse();
                        $.each(headToSort, function(key, value) {
                            head += `<th>${value}</th>`; 
                        });
                        head += '</tr>';
                        rows += '<tr>';
                        let bodyToSort = [];
                        $.each(data, function(key, value) {
                            for (const key in value) {
                                bodyToSort.push(value[key]);
                            }
                        });
                        // Ordenamos en orden alfabetico inverso
                        bodyToSort.sort().reverse();
                        $.each(bodyToSort, function(key, value) {
                            rows += `<td>${value}</td>`;
                        });
                        rows += '</tr>';
                        // Carga la subtabla de datos similares en los demas locations de la base de datos
                        generaConsulta(field,id);
                        break;
                    case '_Metadata':
                        head += '<tr>';
                        $.each(data, function(key, value) {
                            for (const key in value) {
                                head += `<th>${key}</th>`;
                            }
                            return false;
                        });
                        head += '</tr>';
                        $.each(data, function(key, value) {
                            rows += '<tr>';
                            for (const key in value) {
                                rows += `<td>${value[key]}</td>`;
                            }
                            rows += '</tr>';
                        });
                        break;
                    default:
                        break;
                }
                // Add rows to table
                $('#resultadosTabla thead').html(head);
                $('#resultadosTabla tbody').html(rows);

            } else {
                toast("No data available");
            }
        }, {
            onlyOnce: true
        }).catch((error) => {
            console.error(error);
        });
    }

    // genera la consulta
    function generaConsulta(field, id) {
        isloading(1);
        get(ref(db)).then((snapshot) => {
            if (snapshot.exists()) {
                const data = snapshot.val();
                const locations = Object.keys(data);
                // Muestra la card-info
                $('.card-info').removeClass('is-hidden');
                // Cambia el title
                $('.card-info .card-header-title').html('Registros Disponibles: ' + locations.length);
                let template = '<ul>';
                $.each(locations, function(key, value) {
                    // Alimentala con los nombres de las locations
                    template += '<li>';
                        template += `<span class="is-clickable query-locations" data-field='${field}' data-id='${id}'>${value}</span>`;
                    template += '</li>';
                })
                $('.card-info .card-content').html(template);

                // Query locations
                $('.query-locations').on('click', function() {
                    const location = $(this).text();
                    const field = $(this).attr('data-field');
                    const id = $(this).attr('data-id');
                    const que = query(ref(db, location), orderByChild(field), equalTo(id));
                    // Consultas a Firebase con Promise
                    consultasFirebase(que).then(function(result) {
                        // Lee result
                        
                    }).catch(function(error) {
                        console.log(error);
                    });
                });
                isloading(0);
            } else {
                isloading(0);
                toast("No data available");
            }
        }, {
            onlyOnce: true
        }).catch((error) => {
            console.error(error);
        });
    }


    // Reporte Total
    $('#reporteTotal').on('click', function(e) {
        e.preventDefault();
        // Obtenemos los lacation disponibles
        get(ref(db)).then((snapshot) => {
            if (snapshot.exists()) {
                $('.select').removeClass('is-loading');
                const data = snapshot.val();
                const locations = Object.keys(data);
                $.each(locations, function(key, value) {
                    //console.log(value);
                    const que = query(ref(db, value));
                    // Consultas a Firebase con Promise
                    consultasFirebase(que).then(function(result) {
                        
                    }).catch(function(error) {
                        console.log(error);
                    });
                })
                
            } else {
                toast("No data available");
            }
        }, {
            onlyOnce: true
        }).catch((error) => {
            console.error(error);
        });
        
    });
    
    function consultasFirebase(query) {
        // Return a Promise to allow asynchronous processing
        return new Promise(function(resolve, reject) {
            // Get the data from the database
            get(query).then((snapshot) => {
                if (snapshot.exists()) {
                    const data = snapshot.val();
                    console.log(data)
                    resolve(data);
                } else {
                    toast("No data available");
                }
            }, {
                onlyOnce: true
            }).catch((error) => {
                console.error(error);
            });
            // Resolve the Promise with the modified object
        });
    }


}());