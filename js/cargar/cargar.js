// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.23.0/firebase-app.js";
 // TODO: Add SDKs for Firebase products that you want to use
 // https://firebase.google.com/docs/web/setup#available-libraries

 // Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyC-bZu5_F00-Fmf4L6JgPF0wk4v8VjkoN4",
    authDomain: "testrealdb-17286.firebaseapp.com",
    projectId: "testrealdb-17286",
    storageBucket: "testrealdb-17286.appspot.com",
    messagingSenderId: "974668284905",
    appId: "1:974668284905:web:4b352bdf6f22d3b01c0178"
};

import { getDatabase, ref, set, get, update, remove, child, query } from "https://www.gstatic.com/firebasejs/9.23.0/firebase-database.js";

// documento on ready
$(function() {

     // Initialize Firebase
    const app = initializeApp(firebaseConfig);

    // Get a reference to the database service
    const db = getDatabase();

    // Carga CSV Archivo 
    $('.carga-csv').on('change',function() {
        loadCSVFileCheck($(this));
    });

    // Inicializa el result como array, el fileName y tipoCaso
    let exportObject = undefined;
    let jsonName = '', header = '', subHeader = '', subSubHeader = '';
    let returnedArray = [], paisObjeto = [], regionObjeto = [], incomeObjeto = [];
    
    //cargaLocationsAvailable();
    
    // Procesa eo exportObject a los diferentes locations desde button procesaJson
    $('#procesaJson').on('click', function(e) {
        let content = undefined;
        header = '';
        // Vaciamos returnedArray
        returnedArray = [];
        // Usamos el exportObject
        content = exportObject.data;
        // Extramos el segundo indice como key y el primer indice como value de cada array de content
        // paises
        paisObjeto = content.reduce((acc, val) => {
            acc[val[1]] = val[0];
            return acc;
        }, {});
        // region
        regionObjeto = content.reduce((acc, val) => {
            acc[val[1]] = val[2];
            return acc;
        }, {});
        // income_group
        incomeObjeto = content.reduce((acc, val) => {
            acc[val[1]] = val[3];
            return acc;
        }, {});
        // Recorre el array de key 4 hasta 65 y lo agrega al array de anios
        let arrAnios = {}, arrName = {}, arrPoblation = {};
        const keysPaises = Object.keys(paisObjeto);
        let n = 0;
        for(let i = 1960; i <= 2021; i++) {
            arrAnios[i] = i;
        }
        // poblation_groups
        const keysAnios = Object.keys(arrAnios);
        let poblationObjeto = content.reduce((acc, val) => {
            const key = val[1];
            const values = val.slice(4);
            acc[key] = values.reduce((obj, value, index) => {
                obj[keysAnios[index]] = value;
                return obj;
            }, {});
            //acc[val[1]] = val.slice(4);
            return acc;
        }, {});
        // Obtenemos el nombre de las llaves del json en base a los tags data
        $('.tag-names').each(function() {
            $('.textarea').addClass('is-disabled');
            header = $(this).data('key') + '/';
            // De acuerdo a los tags es el destino en la bdd de firebase
            subHeader = $(this).data('name') + '/';
            returnedArray = poblationObjeto;
            $('.textarea, .save-button').addClass('is-disabled');
            $('#paisesContent, #btnGuardar').removeClass('is-disabled');
            if ($(this).data('val')) {
                subSubHeader = $(this).data('val');
            }
            if ($(this).data('key') == 'Paises') {
                $('.textarea, #paisesContent, .save-button').removeClass('is-disabled');
                $('#btnGuardar').addClass('is-disabled');
            }
        })
        // Listado de clave pais y nombre pais || region
        const jsonString = JSON.stringify(paisObjeto, null, 5);
        $('#paisesNombreClave').val(jsonString);
        const jsonStringRegion = JSON.stringify(regionObjeto, null, 5);
        $('#paisesRegion').val(jsonStringRegion);
        const jsonStringIncome = JSON.stringify(incomeObjeto, null, 5);
        $('#paisesIncomeGroup').val(jsonStringIncome);
        const jsonStringContent = JSON.stringify(returnedArray, null, 5);
        $('#paisesContent').val(jsonStringContent);
        // Listado de anios
        //console.log(arrAnios);
    })

    // Guarda los datos en la base de datos
    $('#btnGuardar').on('click', function(e) {
        // Remueve los valores vacios
        removeEmptyValues(returnedArray).then(function(result) {
            // Guarda en firebase
            const dbRef = ref(db,header + subHeader + subSubHeader);
            update(dbRef, result).then(() => {
                $('.file-name').html('');
                $('#btnGuardar, #procesaJson, .save-button').addClass('is-disabled');
                $('#contentReader').html('')
                toast('Registrados','success');
            }).catch((error) => {
                toast(error,'danger');
                console.error(error);
            });
        }).catch(function(error) {
            console.log(error);
        });
        
    })

    // Save Buttons para guardar los datos en la base de datos de acuerdo al dataid
    $('.save-button').on('click', function(e) {
        const subKey = $(this).data('id');
        let sendArray = [];
        switch (subKey) {
            case 'country':
                sendArray = paisObjeto;
                break;
            case 'income':
                sendArray = incomeObjeto;
                break;
            case 'region':
                sendArray = regionObjeto;
                break;
        
            default:
                break;
        }
        // Remueve los valores vacios
        removeEmptyValues(sendArray).then(function(result) {
            // Guarda en firebase
            const dbRef = ref(db,header + subKey);
            update(dbRef, result).then(() => {
                $('.file-name').html('');
                $('#btnGuardar, #procesaJson').addClass('is-disabled');
                $('#contentReader').html('')
                toast('Registrados','link');
            }).catch((error) => {
                toast(error,'danger');
                console.error(error);
            });
        }).catch(function(error) {
            console.log(error);
        });
        
    })

    // Revisa el archivo CSV y lo carga en el DOM
    function loadCSVFileCheck(fileInput) {
        jsonName = '';
        let fileName = fileInput.val();
        let extension = fileName.split('.').pop().toLowerCase();
        // Tags identificadores
        let names = {
            'SP.POP.0014.FE':
                '<span data-key="Edades" data-name="0014" data-val="FE" class="tag is-small m-1 is-info tag-names">edad 0 a 14 Femenino</span>',
            'SP.POP.0014.MA':
                '<span data-key="Edades" data-name="0014" data-val="MA" class="tag is-small m-1 is-info tag-names">edad 0 a 14 Masculino</span>',
            'SP.POP.0014.TO_':
                '<span data-key="Edades" data-name="0014" data-val="TOTL" class="tag is-small m-1 is-info tag-names">edad 0 a 14 Total</span>',
            'SP.POP.1564.FE':
                '<span data-key="Edades" data-name="1564" data-val="FE" class="tag is-small m-1 is-info tag-names">edad 15 a 64 Femenino</span>',
            'SP.POP.1564.MA':
                '<span data-key="Edades" data-name="1564" data-val="MA" class="tag is-small m-1 is-info tag-names">edad 15 a 64 Masculino</span>',
            'SP.POP.1564.TO_':
                '<span data-key="Edades" data-name="1564" data-val="TOTL" class="tag is-small m-1 is-info tag-names">edad 15 a 64 Total</span>',
            'SP.POP.65UP.FE':
                '<span data-key="Edades" data-name="65UP" data-val="FE" class="tag is-small m-1 is-info tag-names">edad 65 y m&aacute;s Femenino</span>',
            'SP.POP.65UP.MA':
                '<span data-key="Edades" data-name="65UP" data-val="MA" class="tag is-small m-1 is-info tag-names">edad 65 y m&aacute;s Masculino</span>',
            'SP.POP.65UP.TO_':
                '<span data-key="Edades" data-name="65UP" data-val="TOTL" class="tag is-small m-1 is-info tag-names">edad 65 y m&aacute;s Total</span>',
            'SP.POP.TOTL.FE':
                '<span data-key="Sexo" data-name="FE" data- class="tag is-small m-1 is-info tag-names">Total Femenino</span>',
            'SP.POP.TOTL.MA':
                '<span data-key="Sexo" data-name="MA" class="tag is-small m-1 is-info tag-names">Total Masculino</span>',
            'SP.POP.TOTL_':
                '<span data-key="Sexo" data-name="TOTL" class="tag is-small m-1 is-info tag-names">Total Poblacion</span>',
            'Metadata_Country_':
                '<span data-key="Paises" class="tag is-small m-1 is-info tag-names">Metadata Country</span>'
        };
        // 
        // Crea el nombre del json
        let cabeceraTexto = 'Basado en el titulo parece contener:';
        $.each(names, function(index, value) {
            if (fileName.indexOf(index) != -1) {
                cabeceraTexto += value;
                jsonName += '_' + index;
            }
        })
        $('.file-tags').html(cabeceraTexto + '<span class="tag">' + jsonName + '</span>');

        if (extension != 'csv') {
            toast('Solo CSV','danger');
            $('.file-name').html('<span class="has-text-weight-bold has-text-danger">Archivo no v&aacute;lido<span>');
            $('.boton-guarda').addClass('is-disabled');
            $('#contentReader').html('')
        } else {
            toast('Si es CSV');
            $('.file-name').html('<span class="has-text-weight-bold has-text-success">' + fileName + '</span>');
            //$('.boton-guarda').removeClass('is-disabled');
            $('#contentReader').html('');
            // Carga el archivo CSV en JsonFormat
            loadCSVFileDOM();
        }
    }

    // Carga el CSV en el DOM y los conveirte en JSON
    function loadCSVFileDOM(e) {
        const file = $('.carga-csv').prop('files')[0];
        let txt = '';

        // Carga el archivo CSV con Papa
        Papa.parse(file, {
            complete: function(results) {
                // Guarda los resultados en el array exportObject
                exportObject = results;
                // Recorre el array de resultados y los muestra en el DOM
                $.each(exportObject.data, function(index, value) {
                    // Creamos el texto a mostrar
                    txt += '<p class="is-size-7 paraIndex-' + index + '">';
                        txt += '<span class="icon has-text-danger-dark is-clickable borra-registro" data-id="' + index + '"><ion-icon name="remove-circle"></ion-icon></span>';
                    $.each(value, function(key, item) {
                        txt += '<span>' + item + ', </span>'
                    })
                    txt += '</p>'
                })
                // Cambiamos el tema del div
                $('.cargar-content-show').css('height', '250px');
                $('#contentReader').html(txt);
                // habilita el boton de procesaJson
                $('#procesaJson').removeClass('is-disabled');

                $('.borra-registro').on('click', function(e) {
                    let id = $(this).data('id');
                    // Crea un modal para confirmar el borrado
                    let mTxt = '<div class="card-header has-text-centered">';
                        mTxt += '<span class="card-header-title">Quieres borrar el registro ' + id + '?</span>'
                    mTxt += '</div>';
                    mTxt += '<div class="card-content has-text-centered">';
                        mTxt += '<button class="button is-danger" id="borrarRegistro">Borrar!</button>';
                    mTxt += '</div>';
                    modal(mTxt);
                    // Si se confirma el borrado
                    $('#borrarRegistro').on('click', function(e) {
                        isloading(1);
                        if (delete exportObject.data[id]) {
                            toast('Registro borrado', 'success');
                            $('.paraIndex-' + id).remove();
                            isloading(0);
                        } else {
                            toast('No se pudo borrar el registro', 'danger');
                            isloading(0);
                            return false;
                        }
                        modax();
                        //console.log(exportObject.data);
                    })
                })
            }
        })
    }

    // Remueve los datos vacios del objeto
    function removeEmptyValues(obj) {
        // Return a Promise to allow asynchronous processing
        return new Promise(function(resolve, reject) {
            // Get the keys of the object
            let keys = Object.keys(obj);
            // Loop through the keys
            keys.forEach(function(key) {
                // If the value is an object, recursively call the function
                if (typeof obj[key] === 'object') {
                    removeEmptyValues(obj[key]).then(function(result) {
                    obj[key] = result;
                    });
                // If the value is an empty string, delete the key
                } else if (obj[key] === '') {
                    delete obj[key];
                }
            });
            // Resolve the Promise with the modified object
            resolve(obj);
        });
    }


    // Carga los libros disponibles
    function cargaLocationsAvailable(e) {
        isloading(1);
        get(ref(db)).then((snapshot) => {
            if (snapshot.exists()) {
                const data = snapshot.val();
                const locations = Object.keys(data);
                // Muestra la card-info
                $('.card-info').removeClass('is-hidden');
                // Cambia el title
                $('.card-info .card-header-title').html('Registros Disponibles: ' + locations.length);
                let template = '<ul>';
                $.each(locations, function(key, value) {
                    // Alimentala con los nombres de las locations
                    template += '<li style="display: flex; align-items: center; border-bottom: 1px dotted">';
                        template += `<ion-icon class="mr-3 is-clickable delete-info" name="trash" data-id='${key}' data-text='${value}'></ion-icon>`;
                        template += `<span class="mr-3 is-size-7">${value}</span>`;
                        template += `<ion-icon class="is-clickable show-info" name="add-circle" data-id='${key}' data-text='${value}'></ion-icon>`;
                    template += '</li>';
                })
                $('.card-info .card-content').html(template);
                // Borra informacion del dataset de firebase y del DOM
                $('.delete-info').on('click', function(e) {
                    let id = $(this).data('id');
                    let text = $(this).data('text');
                    // Crea un modal para confirmar el borrado
                    let mTxt = '<div class="card-header has-text-centered">';
                        mTxt += '<span class="card-header-title">Quieres borrar el registro ' + text + '?</span>'
                    mTxt += '</div>';
                    mTxt += '<div class="card-content has-text-centered">';
                        mTxt += '<button class="button is-danger" id="borrarRegistro">Borrar?</button>';
                    mTxt += '</div>';
                    modal(mTxt);
                    // Si se confirma el borrado
                    $('#borrarRegistro').on('click', function(e) {
                        remove(query(ref(db, text))).then(() => {
                            cargaLocationsAvailable();
                            modax();
                        }).catch((error) => {
                            console.error(error);
                        });
                    })
                })
                // Muestra la informacion del dataset
                $('.show-info').on('click', function(e) {
                    let id = $(this).data('id');
                    let text = $(this).data('text');
                    // Template for div card class
                    let template = '<div class="card-header has-text-centered">';
                        template += '<span class="card-header-title">Informacion del dataset ' + text + '</span>';
                    template += '</div>';
                    template += '<div class="card-content">';
                    get(query(ref(db, text))).then((snapshot) => {
                        if (snapshot.exists()) {
                            const data = snapshot.val();
                            // Agrega input para filter data
                            template += '<div class="field">';
                                template += '<div class="control is-small">';
                                    template += '<input class="input" type="text" placeholder="Filtrar datos" id="filterData">';
                                template += '</div>';
                            template += '</div>';
                            template += '<div class="dataset-view">';
                            $.each(data, function(key, value) {
                                template += `<ul><li><p class="has-text-weight-bold">${value['Country Name']} - <small>${value['Country Code']}</small></p></li></ul>`;
                                template += '<ul>';
                                $.each(value, function(k, v) {
                                    // Alimentala con los nombres de las locations
                                    template += '<li style="display: flex; align-items: center; border-bottom: 1px dotted">';
                                        template += `<span class="mr-3 is-size-7">${k}</span>`;
                                        template += `<span class="mr-3 is-size-7">${v}</span>`;
                                    template += '</li>';
                                })
                                template += '</ul>';
                            })
                            template += '</div>';
                            modal(template);
                            // Bind the keyup event to the input field
                            $('#filterData').on('keyup', function() {
                                // Get the search string and convert it to lowercase
                                const searchString = $(this).val().toLowerCase();
                                // Use the filter() method to hide the li elements that don't match the search string
                                $('ul li').filter(function() {
                                    return $(this).text().toLowerCase().indexOf(searchString) === -1;
                                }).hide();
                                // Show the li elements that match the search string
                                $('ul li').filter(function() {
                                    return $(this).text().toLowerCase().indexOf(searchString) !== -1;
                                }).show();
                            });
                            // Muestra la informacion del DOM con HR click
                            $('hr').on('click', function(e) {
                                // show() child li
                                $(this).next().toggle();
                            })
                        } else {
                            console.log("No data available");
                        }
                    }).catch((error) => {
                        console.error(error);
                    });
                })
            } else {
                console.log("No data available");
            }
            isloading(0);
        }, {
            onlyOnce: true
        }).catch((error) => {
            console.error(error);
        });
    }
}());